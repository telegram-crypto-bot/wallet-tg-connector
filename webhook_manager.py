import base64
import hashlib
import hmac
import os

from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import JSONResponse

app = FastAPI()


@app.post("/wallet/webhook")
def process_paid_order(request: Request):
    if check_request(request):
        return JSONResponse({})
    raise HTTPException(403)


async def check_request(request: Request) -> bool:
    api_token = get_api_token()
    body = await request.body()

    method = request.method
    path = request.url.path
    timestamp = request.headers.get("walletpay-timestamp")

    body_b64 = base64.b64encode(body).decode()
    text = f"{method}.{path}.{timestamp}.{body_b64}"
    sign = hmac.new(
        key=api_token.encode(),
        msg=text.encode(),
        digestmod=hashlib.sha256,
    ).digest()
    sign_b64 = base64.b64encode(sign).decode()
    got = request.headers.get("walletpay-signature")
    return got == sign_b64


def get_api_token() -> str:
    api_token = os.getenv("WALLET_API_TOKEN")
    if api_token is None:
        raise ValueError
    return api_token
