import logging
from decimal import Decimal
from enum import Enum
from typing import Any, TypeAlias

from aiohttp import ClientSession

logger = logging.getLogger(__name__)


class HTTPMethod(Enum):
    GET = "get"
    POST = "post"


ResponseType: TypeAlias = dict[str, Any]


class WalletAPI:
    def __init__(self, api_token: str):
        self.base_url = "https://pay.wallet.tg"
        self.api_token = api_token

    async def _make_request(
        self, method: HTTPMethod, path: str, params: dict[str, Any] | None = None, data: dict[str, Any] | None = None
    ) -> ResponseType:
        headers = {
            "Wpay-Store-Api-Key": self.api_token,
        }
        async with ClientSession(base_url=self.base_url, headers=headers) as session:
            match method:
                case HTTPMethod.GET:
                    async with session.get(url=path, params=params) as response:
                        json_body = await response.json()
                case HTTPMethod.POST:
                    async with session.post(url=path, json=data) as response:
                        json_body = await response.json()
        return json_body

    async def get_orders_list(self, offset: int, count: int) -> ResponseType:
        method = HTTPMethod.GET
        path = "/wpay/store-api/v1/reconciliation/order-list"
        params = {
            "offset": offset,
            "count": count,
        }
        response = await self._make_request(method, path, params=params)
        logger.debug("Response: %s", response)
        return response

    async def get_orders_amount(self) -> ResponseType:
        method = HTTPMethod.GET
        path = "/wpay/store-api/v1/reconciliation/order-amount"
        response = await self._make_request(method, path)
        logger.debug("Response: %s", response)
        return response

    async def get_order_preview(self, order_id: str) -> ResponseType:
        method = HTTPMethod.GET
        path = "/wpay/store-api/v1/order/preview"
        params = {
            "id": order_id,
        }
        response = await self._make_request(method, path, params=params)
        logger.debug("Response: %s", response)
        return response

    async def create_order(
        self,
        currency_code: str,
        amount: Decimal,
        description: str,
        external_id: str,
        timeout_seconds: int,
        customer_telegram_user_id: int,
        return_url: str | None = None,
        fail_return_url: str | None = None,
        custom_data: str | None = None,
    ) -> ResponseType:
        method = HTTPMethod.POST
        path = "/wpay/store-api/v1/order"

        data = {
            "amount": {
                "currencyCode": currency_code,
                "amount": str(amount),
            },
            "description": description,
            "externalId": external_id,
            "timeoutSeconds": timeout_seconds,
            "customerTelegramUserId": customer_telegram_user_id,
        }
        if return_url is not None:
            data["returnUrl"] = return_url
        if fail_return_url is not None:
            data["failReturnUrl"] = fail_return_url
        if custom_data is not None:
            data["customData"] = custom_data

        response = await self._make_request(method, path, data=data)
        logger.debug("Response: %s", response)
        return response
