from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from enum import Enum


class OrderStatus(Enum):
    ACTIVE = "ACTIVE"
    EXPIRED = "EXPIRED"
    PAID = "PAID"


class CurrencyCode(Enum):
    USD = "USD"


@dataclass
class PaymentOptions:
    fee_currency: CurrencyCode
    fee_amount: Decimal
    net_currency: CurrencyCode
    net_amount: Decimal
    exchange_rate: Decimal


@dataclass
class OrderPreview:
    order_id: int
    status: OrderStatus
    number: str
    currency: CurrencyCode
    amount: Decimal
    created_at: datetime
    expiration_at: datetime
    completed_at: datetime | None
    pay_link: str
    direct_pay_link: str


@dataclass
class Order:
    order_id: int
    status: OrderStatus
    currency: CurrencyCode
    amount: Decimal
    external_id: str
    created_at: datetime
    expiration_at: datetime
    payment_at: datetime | None
    payment_options: PaymentOptions | None
