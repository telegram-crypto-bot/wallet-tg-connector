from datetime import datetime
from decimal import Decimal
from typing import Any

from wallet.exceptions import WalletError
from wallet.order import CurrencyCode, Order, OrderPreview, OrderStatus, PaymentOptions
from wallet.wallet_api import ResponseType, WalletAPI


class WalletConnector:
    def __init__(self, api_token: str):
        self.wallet_api = WalletAPI(api_token)
        self.success_status = "SUCCESS"

    async def get_orders_amount(self) -> int:
        response = await self.wallet_api.get_orders_amount()
        amount = self._parse_orders_amount(response)
        return amount

    async def get_orders(self, offset: int = 0, count: int = 100) -> list[Order]:
        response = await self.wallet_api.get_orders_list(offset, count)
        orders = self._parse_orders(response)
        return orders

    async def get_order(self, order_id: int) -> OrderPreview:
        response = await self.wallet_api.get_order_preview(str(order_id))
        order = self._parse_order(response)
        return order

    async def create_order(
        self,
        amount: Decimal,
        description: str,
        external_id: str,
        timeout_seconds: int,
        customer_telegram_user_id: int,
        currency_code: CurrencyCode = CurrencyCode.USD,
        return_url: str | None = None,
        fail_return_url: str | None = None,
        custom_data: str | None = None,
    ) -> OrderPreview:
        response = await self.wallet_api.create_order(
            currency_code=currency_code.value,
            amount=amount,
            description=description,
            return_url=return_url,
            fail_return_url=fail_return_url,
            custom_data=custom_data,
            external_id=external_id,
            timeout_seconds=timeout_seconds,
            customer_telegram_user_id=customer_telegram_user_id,
        )
        order = self._parse_order(response)
        return order

    def _parse_orders_amount(self, response: ResponseType) -> int:
        if not self._is_successful(response):
            raise WalletError(self._get_message(response))
        amount = response["data"]["totalAmount"]
        return amount

    def _is_successful(self, response: dict[str, Any]) -> bool:
        return response["status"] == self.success_status

    def _get_message(self, response: dict[str, Any]) -> str:
        return response["message"]

    def _parse_orders(self, response: ResponseType) -> list[Order]:
        if not self._is_successful(response):
            raise WalletError(self._get_message(response))
        orders = []
        order_dicts = response["data"]["items"]
        for order_dict in order_dicts:
            order = self._get_order(order_dict)
            orders.append(order)
        return orders

    def _get_order(self, order_dict: dict[str, Any]) -> Order:
        payment_options = self._get_payment_options(order_dict)
        order = Order(
            order_id=order_dict["id"],
            status=OrderStatus(order_dict["status"]),
            currency=CurrencyCode(order_dict["amount"]["currencyCode"]),
            amount=Decimal(order_dict["amount"]["amount"]),
            external_id=order_dict["externalId"],
            created_at=datetime.fromisoformat(order_dict["createdDateTime"]),
            expiration_at=datetime.fromisoformat(order_dict["expirationDateTime"]),
            payment_at=datetime.fromisoformat(order_dict["paymentDateTime"])
            if order_dict.get("paymentDateTime")
            else None,
            payment_options=payment_options,
        )
        return order

    def _get_payment_options(self, order_dict: dict[str, Any]) -> PaymentOptions | None:
        payment_options = None
        options = order_dict.get("selectedPaymentOption", None)
        if options is not None:
            payment_options = PaymentOptions(
                fee_currency=options["amountFee"]["currencyCode"],
                fee_amount=options["amountFee"]["amount"],
                net_currency=options["amountNet"]["currencyCode"],
                net_amount=options["amountNet"]["amount"],
                exchange_rate=options["exchangeRate"],
            )
        return payment_options

    def _parse_order(self, response: ResponseType) -> OrderPreview:
        if not self._is_successful(response):
            raise WalletError(self._get_message(response))
        order_dict = response["data"]
        order = OrderPreview(
            order_id=order_dict["id"],
            status=OrderStatus(order_dict["status"]),
            number=order_dict["number"],
            currency=CurrencyCode(order_dict["amount"]["currencyCode"]),
            amount=Decimal(order_dict["amount"]["amount"]),
            created_at=datetime.fromisoformat(order_dict["createdDateTime"]),
            expiration_at=datetime.fromisoformat(order_dict["expirationDateTime"]),
            completed_at=datetime.fromisoformat(order_dict["completedDateTime"])
            if order_dict.get("completedDateTime")
            else None,
            pay_link=order_dict["payLink"],
            direct_pay_link=order_dict["directPayLink"],
        )
        return order
