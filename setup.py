from setuptools import find_packages, setup

setup(
    name="wallet_tg",
    version="0.1.0",
    packages=find_packages(),
    install_requires=["aiohttp"],
)
