import asyncio
import logging
import os
import sys
import uuid
from decimal import Decimal

from wallet.exceptions import WalletError
from wallet.wallet_connector import WalletConnector

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


async def main() -> None:
    api_token = os.getenv("WALLET_API_TOKEN")
    if api_token is None:
        logger.error("WALLET_API_TOKEN is not provided")
        sys.exit(1)

    connector = WalletConnector(api_token)

    try:
        amount = await connector.get_orders_amount()
        print(f"Orders amount: {amount}")
    except WalletError as exc:
        print(f"Error: {exc.message}")

    try:
        orders = await connector.get_orders()
        print(f"Orders: {orders}")
    except WalletError as exc:
        print(f"Error: {exc.message}")

    try:
        order = await connector.create_order(
            amount=Decimal(0.5),
            description="Test order",
            external_id=str(uuid.uuid4()),
            timeout_seconds=3600,
            customer_telegram_user_id=1,
        )
    except WalletError as exc:
        print(f"Error: {exc.message}")
    print(order)

    order_id = order.order_id
    try:
        order2 = await connector.get_order(order_id)
        print(order2)
    except WalletError as exc:
        print(f"Error: {exc.message}")


if __name__ == "__main__":
    asyncio.run(main())
