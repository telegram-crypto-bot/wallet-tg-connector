# [Wallet](https://wallet.tg/) API connector

[API documentation](https://docs.wallet.tg/pay/)

# Before start

Set `WALLET_API_TOKEN` env:

```bash
WALLET_API_TOKEN = "YOUR TOKEN"
```
